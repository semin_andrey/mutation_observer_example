import React, { useState } from 'react';
import { Button, Layout, Typography } from 'antd';
import { useCountInterval, useMutationObserver } from './hooks';
import { TIME_INTERVAL } from './constants';
import './App.css';

const { Content, Sider } = Layout;
const { Title, Text } = Typography;

const App = () => {
  const [isCounterActive, setIsCounterActive] = useState(false);
  const [isMutationObserverActive, setIsMutationObserverActive] = useState(
    false
  );

  const count = useCountInterval(isCounterActive);
  useMutationObserver(isMutationObserverActive, mutations =>
    console.log('mutations', mutations)
  );

  const handleStartCounterClick = () => {
    setIsCounterActive(true);
  };

  const handleStopCounterClick = () => {
    setIsCounterActive(false);
  };

  const handleStartMutationObserverClick = () => {
    setIsMutationObserverActive(true);
  };

  const handleStopMutationObserverClick = () => {
    setIsMutationObserverActive(false);
  };

  return (
    <div className="App">
      <Layout className="layout">
        <Sider width={500} className="sider whiteBackground">
          <div className="block blockFlex">
            <Button
              type="default"
              onClick={
                isCounterActive
                  ? handleStopCounterClick
                  : handleStartCounterClick
              }
            >
              {isCounterActive ? 'Stop' : 'Start'} counter
            </Button>
            <Button
              type="primary"
              className="primaryButton"
              onClick={
                isMutationObserverActive
                  ? handleStopMutationObserverClick
                  : handleStartMutationObserverClick
              }
            >
              {isMutationObserverActive ? 'Stop' : 'Start'} MutationObserver
            </Button>
          </div>
          <div className="block">
            <Title level={4} className="statusText">
              Counter is{' '}
              <Text className="valueText">
                {isCounterActive ? 'on' : 'off'}
              </Text>
            </Title>
            <Title level={4} className="statusText">
              MutationObserver is{' '}
              <Text className="valueText">
                {isMutationObserverActive ? 'on' : 'off'}
              </Text>
            </Title>
          </div>
        </Sider>
        <Content className="greyBackground block">
          <Title data-count={count} id="observedNode" className="text">
            Current count: {count}
          </Title>
          {isCounterActive && (
            <p>
              <Text className="text">
                Counter is now active! Its value and data- attribute will be
                incremented every {TIME_INTERVAL / 1000} seconds
              </Text>
            </p>
          )}
          {isMutationObserverActive && (
            <p>
              <Text className="text">
                MutationObserver is now active! Open devtools and check the
                console
              </Text>
            </p>
          )}
          {isMutationObserverActive &&
            !isCounterActive && (
              <p>
                <Text className="text">
                  You may want to start the counter in order to see console
                  output
                </Text>
              </p>
            )}
        </Content>
      </Layout>
    </div>
  );
};

export default App;
